# GSheet to Site Test

[![Netlify Status](https://api.netlify.com/api/v1/badges/be4bacdb-eebd-48ee-84a1-fa7d0599d8ca/deploy-status)](https://app.netlify.com/sites/gsheet-to-site-testing/deploys)

This is to test Tabletop.js and see if it's doable in a short span of time.

Link to the Google Sheets used in the example can be found here: https://docs.google.com/spreadsheets/d/1_gBUBbYyftp-mlAevy9g1MHdnczmn7T-ppCzH0AkJ1Q/edit?usp=sharing

Tabletop.js: https://github.com/jsoma/tabletop
